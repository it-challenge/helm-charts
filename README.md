# Helm Charts Moonrepo

If additional CI work is the #1 (initial) side-effect of moving to a monorepo, then dealing with versioning, tagging, and releasing is a close #2.

## The Challenge

I've been trying to manage multiple repos that have in a way multi projects inside which you can call a monorepo in a sense, and all of the expected stuff happened — CI, docs, scripts, and whatnot had to be updated to work with the monorepo directory structure everytime there had to be a refactor or change. However, one thing we did not anticipate is the amount of time we’ll need to spend on figuring out and fixing our version, tag, and release process.

This was our list of requirements:

- Each individual component needs to have its own version using a centralized file that has the versions of each chart.
- All released components need to be tagged according to its stability.
- It should be possible to have a version for the “whole” monorepo.
  – As in, a single version that can be used as a point-in-time reference for the state of the entire repository.

This list of demands is fairly normal but as it turns out, it can be a serious pain in the ass. So here follows a strategy that did work for us + the problems you’re likely to hit + workarounds for those problems.

But… i had to come up with this. I decided against this for a few reasons:

- We would have to “build the world” every time there is an update to the repo. This is not feasible as the build pipeline i made to handle the release is a set of parallel jobs (32 of them on the real repo).
- We did not want to have a “tag explosion” — the repos are updated fairly often and we would end up with thousands of tags in a very short period.
- It would be difficult to “spot” what is considered a “good” tag that represents a fully working project.

Centralized versions seem simple at first, however they come with a bunch of caveats and gotchas from creating a script that could assign a version to each chart from the centralized version file `helm-chart.properties`.

## The Strategy

I've decided to create a poc with a monorepo build tool called moon, why moon? because it's lightweight and simple to set up and gives you all the goodies you find in tools like nx while using a simple syntax.

### Setting up the repo with moon

**Linux, macOS, WSL :**

In a terminal that supports Bash, run:

```bash
curl -fsSL https://moonrepo.dev/install/moon.sh | bash
```

This will install moon to `~/.moon/bin`. You'll then need to set PATH manually in your shell profile.

```bash
export PATH=$HOME/.moon/bin:$PATH
```

**Initialize the repository :**

Let's scaffold and initialize moon in a repository with the `moon init` command. This should typically be ran at the root, but can be nested within a directory.

```bash
moon init
```

When executed, the following operations will be applied.

- Creates a `.moon` folder with a `.moon/workspace.yml` configuration file.
- Appends necessary ignore patterns to the relative `.gitignore`.
- Infers the version control system from the environment.

**Customization :**

To get moon to run with our helm chart monorepo, we need to configure the workspace.yml file.

```yaml
$schema: 'https://moonrepo.dev/schemas/workspace.json'
projects:
  - 'dev/charts/*' # include all charts in the 'dev/charts' directory or you can set them by directory
  - 'dev/library/*' # include all templates in the 'dev/templates' directory
vcs:
  manager: 'git'
  defaultBranch: 'main'
```

now we can the tasks in the tasks.yml file

```yaml
tasks:
  dependency-build:
    command: "helm dependency build ."
  lint:
    command: "helm lint . --with-subcharts"
    deps:
      - dependency-build
```

now if we run moon like this we could get this output:

![Moon test run](public/moon-demo.gif)

Versioning we can handle within the tasks:

```yaml
  package:
    command: |
      read -r chartVersion < <(grep $project.version $workspaceRoot/helm-chart.properties | cut -d'=' -f2) &&
      helm package . --destination=$workspaceRoot/packages/ --version=$chartVersion
```

since it can understand the chart name from the project name variable `$project` without doing any special voodoo functions and get the version from the `helm-chart.properties` file, and for tags we can just use ci rules.